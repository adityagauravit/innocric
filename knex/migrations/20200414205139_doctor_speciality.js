
exports.up = function(knex, Promise) {
    return knex.schema.createTable('doctor_speciality', function(t) {
        t.increments('id').unsigned().primary();
        t.integer('doctor_id').unsigned().notNullable();
        t.integer('speciality_id').unsigned().notNullable();
        t.dateTime('created_at').notNull();
        t.foreign('doctor_id').references('doctor_id').inTable('doctor');
        t.foreign('speciality_id').references('speciality_id').inTable('speciality');
    });

};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('doctor_speciality');
};
