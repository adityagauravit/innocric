exports.up = function(knex, Promise) {
    return knex.schema.createTable('speciality', function(t) {
        t.increments('speciality_id').unsigned().primary();
        t.string('name').notNull();
        t.text('status').nullable();
        t.dateTime('created_at').notNull();
        t.dateTime('updated_at').nullable();

      
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('speciality');
};