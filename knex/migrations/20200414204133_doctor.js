
exports.up = function(knex, Promise) {
    return knex.schema.createTable('doctor', function(t) {
        t.increments('doctor_id').unsigned().primary();
        t.string('name').nullable();
        t.string('email_id').notNull();
        t.text('profile_image').nullable();
        t.text('age').nullable();
        t.text('gender').nullable();
        t.integer('status').nullable();
        t.dateTime('created_at').notNull();
        t.dateTime('updated_at').nullable();

      
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('doctor');
};
