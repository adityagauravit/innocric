const poolDbConnection = require('../../config/db.js');


module.exports = {

    createNewSpeciality: async function (req,res) {
        try{
            
            const name = req.body.name;
            const createdAt = new Date();
            const updatedAt = new Date();

            if( (name == (undefined || '')) ){ 
                return res.status(400).send({
                    status: "FAIL",
                    message: "Please send the correct input.",
                })
                            
             }

             const q1InputData = {
                name:name,
                status:1,
                created_at: createdAt,
                updated_at: updatedAt
            }
            
            await poolDbConnection.query('INSERT INTO speciality SET ?',[q1InputData]);

            return res.send({
                status: "SUCCESS",
                message: "New Specialty is succefully added.",
            
            })


        } catch(e){
            console.error(e);
        }
        
    },

    updateSpecialtyInfoById: async function(req,res) {

        try{
            specialityId =  req.body.speciality_id;
            name = req.body.name;
            updatedAt = new Date();
    
            if(specialityId  ==  (undefined || '') || name  ==  (undefined || '') ){
                return res.status(400).send({
                    status: "FAIL",
                     message: "Please send the correct input.",
                 })
             }
    
            const q1Statement = 'UPDATE speciality SET `name`=?,`updated_at`=? WHERE speciality_id=?';
    
            await poolDbConnection.query(q1Statement,[name,updatedAt,specialityId]) 

            return res.send({
                   status: "SUCCESS",
                    message: 'The speciality is SUCCESSfully updated.'
            })
        
        } catch(e){
            console.error(e)
        }
       
    },

    getAllSpecialityInfo: async function(req,res){
        try{
                const q1Statement = `SELECT * FROM speciality`;
        
                q1Result = await poolDbConnection.query(q1Statement);
        
                return res.send({
                    status: "SUCCESS",
                    speciality_info: q1Result,
                })

        }catch(e){
            console.error(e);
        }
    },

    getSpecialityById: async function(req, res){
        try{
            
            const specialityId = req.query.speciality_id;

            if(specialityId  ==  (undefined || '') ){
                return res.status(400).send({
                    status: "FAIL",
                    message: "Please send the correct input.",
                })
            }
    
            q1Statement = `SELECT * FROM speciality where speciality_id= ${specialityId}`;
    
            q1Result = await poolDbConnection.query(q1Statement);
    
            return res.send({
                status: "SUCCESS",
                speciality_info: q1Result,
            })
    

        } catch(e){
            console.error(e);

        }
    },

    deleteSpecialityById: async function(req, res){
        try{
            const specialityId = req.body.speciality_id;
            if(specialityId  ==  (undefined || '') ){
                return res.status(400).send({
                    status: "FAIL",
                    message: "Please send the correct input.",
                })
            }
    
            q1Statement = `DELETE FROM speciality where speciality_id= ${specialityId}`;
    
            q1Result = await poolDbConnection.query(q1Statement);
    
            return res.send({
                status: "SUCCESS",
                message: 'The speciality is succefully deleted',
            })

        } catch(e){
            console.error(e)
        }
    },


}   