const poolDbConnection = require('../../config/db.js');

module.exports = {

    createNewDoctor: async function(req,res) {
        try{
            const name = req.body.name;
            const emailId = req.body.email_id;
            const profileImage = req.body.profile_image;
            const age = req.body.age;
            const gender = req.body.gender;
            const specialities = req.body.speciality_categories_id;
            const createdAt = new Date();
            const updatedAt = new Date();

            // console.log(req.body);
            // return;

            if( (emailId === (undefined || '')) || (specialities == undefined) 
                        || (specialities.length == 0) ){ 
                    return res.status(400).send({
                        status: "FAIL",
                        message: "Please send the correct input.",
                })
                            
             }

             const q1InputData = {
                name:name,
                email_id:emailId,
                profile_image:profileImage,
                age:age,
                gender:gender,
                status:1,
                created_at: createdAt,
                updated_at: updatedAt
            }
            
            const q1Result = await poolDbConnection.query('INSERT INTO doctor SET ?',[q1InputData]);

            const doctorId = q1Result.insertId;

            for(i in specialities){
               const q2InputData = {
                    doctor_id: doctorId,
                    speciality_id: specialities[i],
                    created_at: updatedAt,
                }
               
                await poolDbConnection.query('INSERT INTO doctor_speciality SET ?',[q2InputData]);

            }

            return res.send({
                status: "SUCCESS",
                message: "New Doctor is succefully added.",
            })


        } catch(e){
            console.error(e);
        }
        
    },

    updateDoctorProfileById: async function(req,res) {
        try{
            doctorId =  req.body.doctor_id;
            name = req.body.name;
            profileImage = req.body.profile_image;
            age = req.body.age;
            updatedAt = new Date();
    
            if(doctorId  ==  (undefined || '') ){
                return res.status(400).send({
                    status: "FAIL",
                     message: "Please send the correct input.",
                 })
             }
    
            const q1Statement = 'UPDATE doctor SET `name`=?,`age`=?,`updated_at`=? WHERE doctor_id=?';
    
            await poolDbConnection.query(q1Statement,[name,age,updatedAt,doctorId]) 
    
    
            // q1Result = await poolDbConnection.query(q1Statement);
    
            return res.send({
                   status: "SUCCESS",
                    message: 'The profile is successfully updated.'
            })

        }catch(e){
            console.error(e);
        }
       
    
    },

    getAllDoctorInfo: async function(req,res){
        try{
                const q1Statement = `SELECT * FROM doctor`;
        
                q1Result = await poolDbConnection.query(q1Statement);
        
                return res.send({
                    status: "SUCCESS",
                    doctor_info: q1Result,
                })
        }catch(e){
            console.error(e);
        }
    },

    getDoctorInfoById: async function(req, res){
        try{
            
            const doctorId = req.query.doctor_id;
            if(doctorId  ==  (undefined || '') ){
                return res.status(400).send({
                    status: "FAIL",
                    message: "Please send the correct input.",
                })
            }
    
            q1Statement = `SELECT * FROM doctor where doctor_id= ${doctorId}`;
    
            q1Result = await poolDbConnection.query(q1Statement);
    
            return res.send({
                status: "SUCCESS",
                doctor_info: q1Result,
            })
    

        } catch(e){
            console.error(e);

        }
    },

    deleteDoctorById: async function(req, res, next){
        try{
            const doctorId = req.body.doctor_id;
            console.log(doctorId);
            if(doctorId  ===  undefined  ){
               return res.status(400).send({
                    status: "FAIL",
                    message: "Please send the correct input.",
                })
            }
    
            q1Statement = `DELETE FROM doctor where doctor_id= ${doctorId}`;
    
            q1Result = await poolDbConnection.query(q1Statement);
    
            return res.send({
                status: "SUCCESS",
                message: 'The docotor is succefully deleted',
            })

        } catch(e){
            console.error(e)
        }
    },


}           