const bodyParser = require('body-parser');
const multer  = require('multer');
const jsonParser = bodyParser.json();

const doctorController = require('../controllers/DoctorController');
const specialtyController = require('../controllers/SpecialityController');

exports.appRoute = router => {
    router.get('/', function(req, res) {
        res.send( 'welcome to API ');
    });
    
    router.post('/api/doctor', jsonParser, doctorController.createNewDoctor);
    router.get('/api/doctor',jsonParser, doctorController.getAllDoctorInfo);
    router.get('/api/doctor_by_id',jsonParser, doctorController.getDoctorInfoById);
    router.put('/api/doctor', doctorController.updateDoctorProfileById);
    router.delete('/api/doctor', doctorController.deleteDoctorById);


    router.post('/api/speciality', jsonParser, specialtyController.createNewSpeciality);
    router.get('/api/speciality', specialtyController.getAllSpecialityInfo);
    router.get('/api/speciality_by_id', specialtyController.getSpecialityById);
    router.put('/api/speciality', specialtyController.updateSpecialtyInfoById);
    router.delete('/api/speciality', specialtyController.deleteSpecialityById);



}    