const express = require('express'),
    app = express(),
    port = process.env.port || 3000,
    router = express.Router(),
    bootstrap = require("./src/bootstrap"),
    bodyParser = require('body-parser');
    // db = require('./db/kernel.js'),
    path = require('path'),
    cors = require('cors');

app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

bootstrap(app);

app.listen(port, () => console.log(`${port} server started`));


